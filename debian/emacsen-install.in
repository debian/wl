#! /bin/sh
# /usr/lib/emacsen-common/packages/install/@PACKAGE@
set -e

FLAVOR=$1
PACKAGE=@PACKAGE@
PKGSNAME=wl

ELCDIR=/usr/share/$FLAVOR/site-lisp/$PKGSNAME
ELDIR=/usr/share/$PKGSNAME
ELCSTAMP=$ELCDIR/compile-stamp

case $FLAVOR in
    emacs23|emacs22|emacs21|emacs20|emacs19|mule2|*xemacs*)
    exit 0
    ;;
esac
if [ ! -f "/usr/share/$FLAVOR/site-lisp/apel/poe.elc" ]; then exit 0; fi
if [ ! -f "/usr/share/$FLAVOR/site-lisp/flim/mime.elc" ]; then exit 0; fi
if [ ! -f "/usr/share/$FLAVOR/site-lisp/semi/mime-view.elc" ]; then exit 0; fi

if [ -f "$ELCSTAMP" ]; then
    if [ -z "`find "$ELDIR/wl/wl.el" -newer "$ELCSTAMP"`" ]; then
	echo "install/$PACKAGE: already byte-compiled for $FLAVOR, skipped"
	exit 0
    fi
    rm -f "$ELCSTAMP"
fi

LOG="$ELCDIR/CompilationLog"
echo "install/$PACKAGE: byte-compiling for $FLAVOR"

cd "$ELDIR"
LINKS=`echo elmo/*.el utils/*.el wl/*.el wl/wl-news.el.in NEWS* WL-* etc`
if [ ! -d "$ELCDIR" ]; then
    mkdir "$ELCDIR"
    chmod 755 "$ELCDIR"
fi

cd "$ELCDIR"
TOELDIR=../../../$PKGSNAME
rm -f *.elc __myinit.el
for f in $LINKS; do
    ln -sf "$TOELDIR/$f" .
done
FILES=`/bin/ls -1 *.el | grep -E -v '^(bbdb-wl.el|elsp-spamfilter.el|elmo-shimbun.el|wl-news.el)'`

cat > __myinit.el << EOF
(setq inhibit-automatic-native-compilation t)
(setq native-comp-deferred-compilation nil)
(setq comp-enable-subr-trampolines nil)
(setq load-path (cons "/usr/share/$FLAVOR/site-lisp/apel" load-path))
(setq load-path (cons "/usr/share/$FLAVOR/site-lisp/flim" load-path))
(setq load-path (cons "/usr/share/$FLAVOR/site-lisp/semi" load-path))
(setq load-path (cons "." load-path))
(setq byte-compile-warnings t)
(setq WLDIR ".")
EOF
if [ -f elmo-shimbun.el ] && [ -f "/usr/share/$FLAVOR/site-lisp/w3m/shimbun/shimbun.elc" ]; then
    FILES="$FILES elmo-shimbun.el"
    cat >> __myinit.el << EOF
(setq load-path (cons "/usr/share/$FLAVOR/site-lisp/w3m" load-path))
(setq load-path (cons "/usr/share/$FLAVOR/site-lisp/w3m/shimbun" load-path))
EOF
fi

FLAGS="-q -no-site-file -batch -l __myinit.el -l WL-MK -eval"
ARG="(progn (config-wl-package-subr)(make-wl-news))"
echo "$FLAVOR" $FLAGS "$ARG" > "$LOG"
set +e
"$FLAVOR" $FLAGS "$ARG" >> "$LOG" 2>&1
if [ $? -ne 0 ]; then cat "$LOG"; exit 1; fi
set -e

FLAGS="-q -no-site-file -batch -l __myinit.el -f batch-byte-compile"
echo "$FLAVOR" $FLAGS $FILES >> "$LOG"
set +e
"$FLAVOR" $FLAGS $FILES >> "$LOG" 2>&1
if [ $? -ne 0 ]; then cat "$LOG"; exit 1; fi
set -e
gzip -9f "$LOG"
chmod 644 *.elc

rm -f __myinit.el* wl-news.el.in NEWS* WL-*

if [ -f /usr/share/emacsen-common/debian-startup.el ] && \
   [ ! -f "/etc/$FLAVOR/site-start.d/65$PACKAGE.el" ] && \
   [ -f "/etc/emacs/site-start.d/65$PACKAGE.el" ] && \
   [ -d "/etc/$FLAVOR/site-start.d" ]; then
    ln -sf "../../emacs/site-start.d/65$PACKAGE.el" "/etc/$FLAVOR/site-start.d/65$PACKAGE.el"
fi

touch "$ELCSTAMP"
exit 0
